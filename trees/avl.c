#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<stdbool.h>
#include<string.h>

struct node
{
	int key;
	struct node* left;
	struct node* right;
	int height;
};

int max(int a, int b)
{
	return (a>b)?a:b;
}

int height(struct node* aNode)
{
	if(aNode == NULL)
		return 0;
	else
		return (max(height(aNode->left),height(aNode->right))+1);
}

struct node* leftRotate(struct node* x)
{
	struct node* y = x->right;
	struct node* T2 = y->left;

	x->right = T2;
	y->left = x;

	x->height = height(x);
	y->height = height(y);
	return y;
}

struct node* rightRotate(struct node* y)
{
	struct node* x = y->left;
	struct node* T2 = x->right;

	x->right = y;
	y->left = T2;

	x->height = height(x);
	y->height = height(y);

	return x; 
}

struct node* newNode(int key)
{
	struct node* newNode = (struct node*)malloc(sizeof(struct node));
	newNode->key = key;
	newNode->left = NULL;
	newNode->right = NULL;
	newNode->height = 1;
}

int getBalance(struct node* root)
{
	return height(root->left) - height(root->right);
}

struct node* balanceTree(struct node* root, int key)
{
	int balance = getBalance(root);

	//left-left 
	if(balance > 1 && key < root->left->key)
		return rightRotate(root);

	//right-right
	if(balance < -1 && key > root->right->key)
		return leftRotate(root);

	//left-right	
	if(balance > 1 && key > root->left->key)
	{
		root->left = leftRotate(root->left);
		return rightRotate(root);
		
	} 

	//right-left
	if(balance < -1 && key < root->right->key)
	{
		root->right = rightRotate(root->right);
		return leftRotate(root); 
	}

	return root;
}

struct node* insert(struct node* root, int key)
{
	if(root == NULL)
		return newNode(key);

	if(key < root->key)
		root->left = insert(root->left, key);
	else
		root->right = insert(root->right, key);

	root->height = height(root);

	return balanceTree(root, key);
}

struct node* minValue(struct node* root)
{
	if(root == NULL)
		return root;
	while(root->left)
		root = root->left;
	return root;
}

struct node* delete(struct node* root, int key)
{
	if(root == NULL)
		return root;

	if(key < root->key)
		root->left = delete(root->left, key);

	else if(key > root->key)
		root->right = delete(root->right, key);

	else
	{
		struct node* temp;

		if(root->left == NULL)
		{	
			temp = root->right;
			*root = *temp;
			free(temp);
		}

		if(root->right == NULL)
		{
			temp = root->left;
			*root = *temp;
			free(temp);
			
		}

		temp = minValue(root->right);
		root->key = temp->key;
		delete(root->right, key);
	}

	root->height = height(root);
	
	return balanceTree(root, key);
}

void preOrder(struct node* root)
{
	if(root == NULL)
		return;
	printf(" %d,",root->key);
	preOrder(root->left);
	preOrder(root->right);
} 

void main()
{
	struct node* root = NULL;
	root = insert(root, 4);
	root = insert(root, 3);
	root = insert(root, 2);
	root - insert(root, 1);
	preOrder(root);
}
