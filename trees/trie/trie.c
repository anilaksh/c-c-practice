#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<string.h>
#include<stdbool.h>

#define ALPHABET_SIZE 26
#define CHAR_TO_INDEX(c) ((int)c-(int)'a')
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

struct trieNode
{
	struct trieNode* children[ALPHABET_SIZE];
	bool isLeaf;
};

struct trieNode* getNode(void)
{
	int i;
	struct trieNode* new_node = (struct trieNode*)malloc(sizeof(struct trieNode));
	new_node->isLeaf = false;
	for(i=0; i<ALPHABET_SIZE; i++)
		new_node->children[i] = NULL;
	return new_node;
}

void insert(struct trieNode* root, char key[])
{
	int level;
	int index;
	int keyLength = strlen(key);
	struct trieNode* pcrawl = root; 

	for(level = 0; level<keyLength; level++)
	{
		index = CHAR_TO_INDEX(key[level]);
		if(!pcrawl->children[index])
		{
			pcrawl->children[index] = getNode();
		}
		pcrawl = pcrawl->children[index];
	}
	pcrawl->isLeaf = true; 
	
}

bool isLeaf(struct trieNode* pNode)
{
	return pNode->isLeaf;
}

bool isFreeNode(struct trieNode* pNode)
{
	int i;
	for(i = 0; i<ALPHABET_SIZE; i++)
	{
		if(pNode->children[i])
			return false;
	} 	
	return true;
}	

bool search(struct trieNode* root, char key[])
{
	bool result = false;
	int level;
	int index;
	int length = strlen(key);
	struct trieNode* pcrawl = root;

	for(level = 0; level<length; level++)
	{
		index = CHAR_TO_INDEX(key[level]);
		if(!pcrawl->children[index])
			break;
		pcrawl = pcrawl->children[index];
	}
	if(pcrawl->isLeaf && level == length)
		result = true;
	return result;  
}

bool deleteHelper(struct trieNode* pNode, char key[], int level, int length)
{
	if(pNode)
	{
		if(level == length)
		{
			if(pNode->isLeaf)
			{
				pNode->isLeaf = false;
				if(isFreeNode(pNode))
					return true;
			}
			return false; 
		}
		else
		{
			int index = CHAR_TO_INDEX(key[level]);
			if(deleteHelper(pNode->children[index], key, level+1, length))
			{
				free(pNode->children[index]);
				pNode->children[index] = NULL;
				return(!isLeaf(pNode) && isFreeNode(pNode));
			} 
			
		}
	}
	return false;
}

void delete(struct trieNode* root, char key[])
{
	int len = strlen(key);
	deleteHelper(root, key, 0, len);
}

struct trieNode* initialize(void)
{
	return getNode();	
}
 
void main()
{
	char key[][8] = {"the","there","those","a","ball","swim","catch","hobby"};
	struct trieNode* root = initialize();
	int i;
	for(i = 0; i<ARRAY_SIZE(key); i++)
	{
		insert(root, key[i]);
	}
	
	printf("\n%d", search(root, "hobb"));
	printf("\n%d", search(root, "bo"));
	printf("\n%d", search(root, "catcher"));
	printf("\n%d", search(root, "the"));
	printf("\n%d", search(root, "there"));

	delete(root, "there");
	printf("\n%d", search(root, "the"));
        printf("\n%d", search(root, "there"));
}
