#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<stdbool.h>
#include<string.h>

#define ALPHABET_SIZE (26)
#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))
#define CHAR_TO_INDEX(c) ((int)c-(int)'a') 

struct node
{
	struct node* children[ALPHABET_SIZE];
	bool isLeaf;
};

struct node* getNode(void)
{
	struct node* pNode = (struct node*)malloc(sizeof(struct node));
	int index;
	for(index = 0; index<ALPHABET_SIZE; index++)
	{
		pNode->children[index] = NULL;
	}
	pNode->isLeaf = false;
}

struct node* initialize(void)
{
	return getNode();
}

void insert(struct node* root, char key[])
{
	int level;
	int index;
	int length = strlen(key);
	struct node* pcrawl = root;
	for(level = 0; level<length; level++)
	{
		index = CHAR_TO_INDEX(key[level]);
		if(!pcrawl->children[index])
			pcrawl->children[index] = getNode();

		pcrawl = pcrawl->children[index];
	} 
	pcrawl->isLeaf = true;
}

bool search(struct node* root, char key[])
{
	bool result = false;
	int level;
	int index;
	int length = strlen(key);
	struct node* pcrawl = root;

	for(level = 0 ; level<length; level++)
	{
		index = CHAR_TO_INDEX(key[level]);
		if(!pcrawl->children[index])
			break;
		pcrawl = pcrawl->children[index];
	}
	if(pcrawl->isLeaf && length == level)
		result = true; 
	return result;
}

bool isLeaf(struct node* pNode)
{
	return pNode->isLeaf;
}

bool isFreeNode(struct node* pNode)
{
	int index;
	for(index=0; index<ALPHABET_SIZE; index++)
	{
		if(pNode->children[index])
			return false;
	}
	return true;	
}

bool deleteHelper(struct node* pNode, char key[], int level, int length)
{
	if(pNode)
	{
		if(level == length)
		{
			if(pNode->isLeaf)
			{
				pNode->isLeaf = false;
				if(isFreeNode(pNode))
					return true;	
			}
			return false;
		}

		else
		{
			int index = CHAR_TO_INDEX(key[level]);
			if(deleteHelper(pNode->children[index], key, level+1, length))
			{
				free(pNode->children[index]);
				pNode->children[index] = NULL;
			}
			return(!isLeaf(pNode) && isFreeNode(pNode));
		}
	}
	return false;

}

void delete(struct node* root, char key[])
{
	deleteHelper(root, key, 0, strlen(key));
}

char* longestPrefix(struct node* root, char key[])
{
	int index;
	int level;
	int length = strlen(key);

	struct node* pNode = root;
	char* result = (char*)malloc(sizeof(length+1));

	int maxLength = 0; 	

	for(level = 0; level<length; level++)
	{
		index = CHAR_TO_INDEX(key[level]);
		if(pNode->children[index])
			result[level] = key[level];
		else
			break;
		pNode = pNode->children[index];
		if(pNode->isLeaf)
			maxLength = level+1;
			
	}
	result[maxLength] = '\0';
	return result; 	
}

void main()
{
        char key[][8] = {"the","there","those","a","ball","swim","catch","hobby"};
        struct node* root = initialize();
        int i;
        for(i = 0; i<ARRAY_SIZE(key); i++)
        {
                insert(root, key[i]);
        }

        printf("\n%d", search(root, "hobb"));
        printf("\n%d", search(root, "bo"));
        printf("\n%d", search(root, "catcher"));
        printf("\n%d", search(root, "the"));
        printf("\n%d", search(root, "there"));

        //delete(root, "there");
        printf("\n%d", search(root, "the"));
        printf("\n%d", search(root, "there"));

	printf("\n%s", longestPrefix(root, "the"));
	printf("\n%s", longestPrefix(root, "therddrr"));
	printf("\n%s", longestPrefix(root, "there"));
}

