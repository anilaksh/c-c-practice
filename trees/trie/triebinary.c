#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<stdbool.h>

#define BINARY_LENGTH 2
#define ROW

struct node
{
	struct node* children[BINARY_LENGTH];
	bool isLeaf;
};

struct node* getNode(void)
{
	struct node* pNode = (struct node*)malloc(sizeof(struct node));
	int index;
	for(index=0; index<BINARY_LENGTH; index++)
		pNode->children[index] = NULL;
	pNode->isLeaf = false;
}

void printRow(int *a, int r, int n)
{
	printf("\n{");
	int i;
	for(i=0; i<n; i++)
	{
		printf(" %d,", *((a+r*n)+i));
	}
	printf(" }");
}

void printUniqueRows(int *matrix, int rLen, int cLen)
{
	struct node* root = getNode();
	struct node* pNode; 
	int m,n,index;

	for(m=0; m<rLen; m++)
	{
		bool unique = false;
		pNode = root;

		for(n=0; n<cLen; n++)
		{
			index = *((matrix+(m*cLen))+n);
			if(!pNode->children[index])
			{
				pNode->children[index] = getNode();
				unique = true; 
			}
			pNode = pNode->children[index];
		}
		pNode->isLeaf = true;	
		if(unique)
			printRow(matrix, m, cLen);
	}
	
}

void main()
{
	int matrix[4][5] = {{0,0,0,0,1},{1,0,1,1,0},{0,1,0,0,1},{1,0,1,1,0}};
	printUniqueRows((int*)matrix, 4, 5);
}
