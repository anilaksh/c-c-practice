#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<stdbool.h>
#include<string.h>

#define SIZE 27 
#define  MAX 16

struct node
{
	struct node* children[SIZE];
	bool isLeaf;
	char *ipAddr;
};

int charToIndex(char c)
{
	if(c == '.')
		return SIZE-1;
	else
		return c-'a';
}

struct node* getNode(void)
{
	struct node* pNode = (struct node*)malloc(sizeof(struct node));
	int index;
	for(index=0;index<SIZE;index++)
	{
		pNode->children[index]=NULL;
	}
	pNode->ipAddr = NULL;
	pNode->isLeaf = false;
}

void insert(struct node* root, char* ipAddr, char* URL)
{
	int index;
	int level;
	int urlLength = strlen(URL);
	struct node* pNode = root;
	for(level=0;level<urlLength;level++)
	{
		index = charToIndex(URL[level]);
		if(!pNode->children[index]) 
			pNode->children[index] = getNode();
		pNode = pNode->children[index];
	}
	pNode->isLeaf = true;
	pNode->ipAddr = (char*)malloc(strlen(ipAddr)+1);
	strcpy(pNode->ipAddr,ipAddr);
}

char* DNSlookup(struct node* root, char* URL)
{
	int index;
	int level;
	int urlLength = strlen(URL);
	struct node* pNode = root;

	for(level=0;level<urlLength;level++)
	{
		index = charToIndex(URL[level]);
		if(!pNode->children[index])
			return NULL;
		pNode = pNode->children[index]; 
	}
	if(pNode && pNode->isLeaf)
		return pNode->ipAddr;
	return NULL;
} 

void main()
{
	char ipAdd[][MAX] = {"107.108.11.123", "107.109.123.255",
                         "74.125.200.106"};
    	char URL[][50] = {"www.samsung.com", "www.samsung.net",
                      "www.google.in"};
    	int n = sizeof(ipAdd)/sizeof(ipAdd[0]);

	struct node* root = getNode();

	int i;
	for(i=0;i<n;i++)
	{
		insert(root, ipAdd[i], URL[i]); 
	}
	
	printf("\n%s", DNSlookup(root, "www.samsung.com"));
	printf("\n%s", DNSlookup(root, "www.samsung.net"));
	printf("\n%s", DNSlookup(root, "www.google.in"));
}


