#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<stdbool.h>
#include<string.h>

#define SIZE 11
#define  MAX 16

struct node
{
	struct node* children[SIZE];
	bool isLeaf;
	char *URL;
};

int charToIndex(char c)
{
	if(c == '.')
		return SIZE-1;
	else
		return c-'0';
}

struct node* getNode(void)
{
	struct node* pNode = (struct node*)malloc(sizeof(struct node));
	int index;
	for(index=0;index<SIZE;index++)
	{
		pNode->children[index]=NULL;
	}
	pNode->URL = NULL;
	pNode->isLeaf = false;
}

void insert(struct node* root, char* ipAddr, char* URL)
{
	int index;
	int level;
	int ipLength = strlen(ipAddr);
	struct node* pNode = root;
	for(level=0;level<ipLength;level++)
	{
		index = charToIndex(ipAddr[level]);
		if(!pNode->children[index]) 
			pNode->children[index] = getNode();
		pNode = pNode->children[index];
	}
	pNode->isLeaf = true;
	pNode->URL = (char*)malloc(strlen(URL)+1);
	strcpy(pNode->URL,URL);
}

char* reverseDNS(struct node* root, char* ipAddr)
{
	int index;
	int level;
	int ipLength = strlen(ipAddr);
	struct node* pNode = root;

	for(level=0;level<ipLength;level++)
	{
		index = charToIndex(ipAddr[level]);
		if(!pNode->children[index])
			return NULL;
		pNode = pNode->children[index]; 
	}
	if(pNode && pNode->isLeaf)
		return pNode->URL;
	return NULL;
} 

void main()
{
	char ipAdd[][MAX] = {"107.108.11.123", "107.109.123.255",
                         "74.125.200.106"};
    	char URL[][50] = {"www.samsung.com", "www.samsung.net",
                      "www.google.in"};
    	int n = sizeof(ipAdd)/sizeof(ipAdd[0]);

	struct node* root = getNode();

	int i;
	for(i=0;i<n;i++)
	{
		insert(root, ipAdd[i], URL[i]); 
	}
	
	printf("\n%s", reverseDNS(root, "107.108.11.123"));
	printf("\n%s", reverseDNS(root, "107.109.123.255"));
	printf("\n%s", reverseDNS(root, "74.125.200.10"));
}


