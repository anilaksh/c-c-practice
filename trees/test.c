#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<stdbool.h>
#include<string.h>

struct node
{
        int key;
	int data;
};

void main()
{

	struct node* root = (struct node*)malloc(sizeof(struct node));
	struct node dum;
	struct node* temp = &dum;
	root->key = 5;
	root->data = 10;

	*temp = *root;

	printf("\n%d %d", temp->key, temp->data);
}
