#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "trees.h"

struct node* newNode(int data)
{
	struct node *newNode = NULL;
	newNode = (struct node*)malloc(sizeof(struct node)); 
	newNode->data = data;
	newNode->left = NULL;
	newNode->right = NULL;
}

struct node* insert(struct node* root, int data)
{
	if(root == NULL)
		return newNode(data);
	if(data <= root->data)
		root->left = insert(root->left, data);
	else
		root->right = insert(root->right, data);
	return root; 
}


int lookup(struct node* root, int data)
{
	if(root == NULL)
		return 0;
	if(data == root->data)
		return 1;
	if(data < root->data)
		return lookup(root->left,data);
	else
		return lookup(root->right,data);
}

void printTree(struct node* root)
{
	if(root == NULL)
		return;
	printTree(root->left);
	printf(" %d, ", root->data);
	printTree(root->right);
}

struct node* minValueNode(struct node* root)
{
	if(root == NULL)
		return NULL;
	struct node* curr = root;
	while(curr->left != NULL)
	{
		curr = curr->left;
	}
	return curr;
}

struct node* maxValueNode(struct node* root)
{
	if(root == NULL)
		return NULL;
	struct node* curr = root;
	while(curr->right != NULL)
		curr = curr->right;

	return curr;
}

struct node* deleteTree(struct node* root, int key)
{
	if(root == NULL)
		return NULL;

	if(key < root->data)
		root->left = deleteTree(root->left, key);
	else if(key > root->data)
		root->right = deleteTree(root->right, key);
	else
	{
		struct node* temp;
		if(root->left == NULL)
		{
			temp = root->right;
			free(root);
			return temp;	
		}
		if(root->right == NULL)
		{
			temp = root->left;
			free(root);
			return temp;
		}
	
		temp = minValueNode(root->right);
		root->data = temp->data;
		root->right = deleteTree(root->right, temp->data);
	}	
	return root;
}

int size(struct node* root)
{
	if(root == NULL)
		return 0;
	return size(root->left)+size(root->right)+1;
}

void inorderPredSuccUtil(struct node* root, struct node** pred, struct node** succ, int key)
{
	if(root == NULL)
		return;
	if(root->data == key)
	{
		if(root->left != NULL)
			*pred = maxValueNode(root->left);
		if(root->right != NULL)
			*succ = minValueNode(root->right);
		return;	
	}
	if(key < root->data)
	{
		*succ = root;
		inorderPredSuccUtil(root->left, pred, succ, key);
	}
	else
	{
		*pred = root;
		inorderPredSuccUtil(root->right, pred, succ, key); 
	}
	
}

void inorderPredSucc(struct node* root, int key)
{
	struct node* pred = NULL;
	struct node* succ = NULL;
	inorderPredSuccUtil(root, &pred, &succ, key);	
	printf("\n\n");
	printf("inorder predecessor of %d is %d", key, pred->data);
	printf("\ninorder successor of %d is %d", key, succ->data);
}

struct node* inorderSucc(struct node* root, struct node* n)
{
	if(root == NULL)
		return NULL;
	if(n->right != NULL)
		return minValueNode(n->right);
	struct node* succ = NULL;
	
	while(root != NULL)
	{
		if(n->data < root->data)
		{
			succ = root;
			root = root->left;
		}
		else if(n->data > root->data)
		{
			root = root->right;
		}
		else
			break;
	}
	return succ; 
} 

struct node* inorderPred(struct node* root, struct node* n)
{
	if(root == NULL)
		return NULL;
	if(n->left != NULL)
		return maxValueNode(n->left);
	struct node* pred = NULL;

	while(root!=NULL)
	{
		if(n->data > root->data)
		{
			pred = root;
			root = root->right;
		}
		else if(n->data < root->data)
		{
			root = root->left;
		}
		else
			break;
	}
	return pred;
}

struct node* LCARecur(struct node* root, struct node* n1, struct node* n2)
{
	if(root == NULL)
		return NULL;
	if(n1->data < root->data && n2->data < root->data)
		return LCARecur(root->left, n1, n2);
	else if(n1->data > root->data && n2->data > root->data)
		return LCARecur(root->right, n1, n2); 
	else
		return root;
} 

struct node* LCA(struct node* root, struct node*n1, struct node* n2)
{
	if(root == NULL)
		return NULL;
	while(root != NULL)
	{
		if(n1->data < root->data && n2->data < root->data)
			root = root->left;
		else if(n1->data > root->data && n2->data > root->data)
			root = root->right;
		else
			break;
	}
	return root;
}

void printSortedOrder(int a[], int start, int end)
{
	if(start > end)
		return;
	printSortedOrder(a, 2*start+1, end);
	printf(" %d, ", a[start]);
	printSortedOrder(a, 2*start+2, end);
}

void kthSmallestElementUtil(struct node* root, int k, int* i)
{
	if(root == NULL)
		return;

	if(*i > k)
		return;

	kthSmallestElementUtil(root->left, k, i);
	
	(*i)++;
	
	printf("\n %d, ", (*i)); 
	if(k == (*i))
	{
		printf("\nkth smallest ele is: %d", root->data);
		return;
	}

	kthSmallestElementUtil(root->right, k, i);
} 

void kthSmallestElement(struct node* root, int k)
{
	int i = 0;
	
	kthSmallestElementUtil(root, k, &i);
}

void kthLargestElementUtil(struct node* root, int k, int* i)
{
	if(root == NULL)
		return;

	if((*i) > k)
		return;

	kthLargestElementUtil(root->right, k, i);
	(*i)++;

	if(k == (*i))
	{
		printf("\nk is: %d and kth largest element is: %d", k, root->data);
		return;
	}

	kthLargestElementUtil(root->left, k, i);
}

void kthLargestElement(struct node* root, int k)
{
	int i = 0;
	kthLargestElementUtil(root, k, &i);
}

void printKeysInRange(struct node* root, int k1, int k2)
{
	if(root == NULL)
		return;

	if(k1 < root->data )
		printKeysInRange(root->left, k1, k2);
	if(k1 <= root->data && root->data <= k2) 
		printf(" %d,", root->data);
	if(k2 > root->data) 
		printKeysInRange(root->right, k1, k2);
}

struct node* sortedArrayToBST(int arr[], int st, int end)
{
	if(st > end)
		return NULL;
	int mid = (st + end)/2;
	struct node* node = newNode(arr[mid]);

	if(st == end)
		return node;

	node->left = sortedArrayToBST(arr, st, mid-1);
	node->right = sortedArrayToBST(arr, mid+1, end);

	return node;
}

void addGreaterNodesUtil(struct node* root, int* sum)
{
	if(root == NULL)
		return;
	addGreaterNodesUtil(root->right, sum);
	root->data = root->data+(*sum);
	*sum = root->data;
	addGreaterNodesUtil(root->left, sum);
}

void addGreaterNodes(struct node* root)
{
	int sum = 0;
	addGreaterNodesUtil(root, &sum);
}

struct node* removeNodesOutsideRange(struct node* root, int min, int max)
{
	if(root == NULL)
		return NULL;
	root->left = removeNodesOutsideRange(root->left, min, max);
	root->right = removeNodesOutsideRange(root->right, min, max);

	if(root->data < min)
	{
		struct node* rchild = root->right;
		free(root);
		return rchild;
	}
	if(root->data > max)
	{
		struct node* lchild = root->left;
		free(root);
		return lchild;
	} 
	return root;
}

void greaterSumTreeUtil(struct node* root, int* sum)
{
	if(root == NULL)
		return;
	greaterSumTreeUtil(root->right, sum);
	(*sum) += root->data;
	root->data = (*sum)-root->data;
	greaterSumTreeUtil(root->left, sum);
}

void greaterSumTree(struct node* root)
{
	int sum = 0;
	greaterSumTreeUtil(root, &sum);
} 

int* merge(int a[], int b[], int m, int n)
{
	int* c = (int*)malloc(sizeof(int)*(m+n)); 

	int i = 0,j = 0,k = 0;
	while(j < m && k < n)
	{
		if(a[j] < b[k])
			c[i++] = a[j++];
		else
			c[i++] = b[k++];
	}
	while(j < m)
		c[i++] = a[j++];
	while(k < n)
		c[i++] = b[k++];
	return c;
		
}

void storeInorderToArray(struct node* root, int arr[], int* index)
{
	if(root == NULL)
		return;

	storeInorderToArray(root->left, arr, index);
	
	arr[*index] = root->data;
	(*index)++;

	storeInorderToArray(root->right, arr, index);
}


struct node* mergeBST(struct node* tree1, struct node* tree2)
{
	int m;
	m = size(tree1);
	int index = 0;
	int *a = (int*)malloc(sizeof(int)*m);
	storeInorderToArray(tree1, a, &index);
	int i;

	int n;
	n = size(tree2);
	index = 0;
	int *b = (int*)malloc(sizeof(int)*n);
	storeInorderToArray(tree2, b, &index); 

	int *c = merge(a, b, m, n);
	
	return sortedArrayToBST(c, 0, m+n-1); 
}

int hasOnlyOneChild(int pre[], int n)
{
	int i, nextdiff, lastdiff;
	for(i=0; i<n-1; i++)
	{
		nextdiff = pre[i] - pre[i+1];
		lastdiff = pre[i] - pre[n-1]);
		if(nextdiff * lastdiff < 1)
			return 0;
			
	}
	return 1;
} 

void main()
{

	struct node* tree = NULL;
        tree = insert(tree,4);
        insert(tree,2);
        insert(tree,3);
        insert(tree,1);
        insert(tree,6);
        insert(tree,7);
        insert(tree,5);

        printTree(tree);
	//tree = deleteTree(tree,6);
	//printf("\n\n");
	//printTree(tree);

//	inorderPredSucc(tree, 6);

//	int a[] = {4,2,6,1,3,5,7};
	printf("\n\n");
//	printSortedOrder(a, 0, 6); 

//	kthSmallestElement(tree, 3);
//	printf("elsements in the given range: ");

//	printKeysInRange(tree, 3, 7);

//	struct node* a = NULL;
//	int arr[] = {10, 20, 30, 40, 50}; 
//	a = sortedArrayToBST(arr, 0, 4);
//	printTree(a);

//	addGreaterNodes(tree);
//	printTree(tree);

//	tree = removeNodesOutsideRange(tree, 4 , 6);
	//greaterSumTree(tree);
	//printTree(tree);

	kthLargestElement(tree, 5);

	
	struct node* tree1 = NULL;
	struct node* tree2 = NULL;
        tree1 = insert(tree1,40);
        insert(tree1,0);

	tree2 = mergeBST(tree, tree1);
	printf("\n\n");
	printTree(tree2);

	

}
