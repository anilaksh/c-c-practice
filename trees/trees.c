#include <stdio.h>
#include <stdlib.h>
#include "trees.h"

struct node* newNode(int data)
{
	struct node *newNode = (struct node *)malloc(sizeof(struct node)); 
	newNode->data = data;
	newNode->left = NULL;
	newNode->right = NULL;	 
	return(newNode);
}

struct node* insert(struct node *root, int data)
{
	struct node *newnode = newNode(data);
	
	if(root == NULL)
		return(newnode);
	if(data < root->data)
		root->left = insert(root->left, data);
	else
		root->right = insert(root->right, data);
	return(newnode);

}

void printTree(struct node* root)
{
	if(root == NULL)
		return;
	printTree(root->left);
	printf("%d,",root->data);
	printTree(root->right);
}

int lookup(struct node* root, int data)
{
	if(root == NULL)
		return(0);
	if(root->data == data)
		return(1);
	if(data < root->data) 
		return lookup(root->left, data);
	else
		return lookup(root->right, data);
}

void main()
{

	struct node *tree;
	struct node *root;
	root = insert(root, 2);
	root->left = insert(root, 1);
	root->right = insert(root, 3);
	printTree(root);
	printf("%d,%d", lookup(root,2), lookup(root,4));
} 
