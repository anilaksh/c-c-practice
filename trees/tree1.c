#include <stdio.h>
#include <stdlib.h>

struct node
{
	int data;
	struct node* left;
	struct node* right;
	struct node*next;
};

struct node* newNode(int data)
{
        struct node* newNode = (struct node*)malloc(sizeof(struct node));
        newNode->data = data;
        newNode->left = NULL;
        newNode->right = NULL;
	newNode->next = NULL;
        return newNode;
}

struct node* insert(struct node* root, int data)
{
        if(root == NULL)
        return newNode(data);
        if(data <= root->data)
                root->left = insert(root->left, data);
        else
                root->right = insert(root->right, data);
        return root;
}

void populateInorderUtil(struct node* root, struct node** next)
{
	if(root)
	{
		populateInorderUtil(root->right, next);
		root->next = (*next);
		(*next) = root;
		populateInorderUtil(root->left, next);
	}
}

void populateInorder(struct node* root)
{
	struct node *next = NULL;
	populateInorderUtil(root, &next);
}

void printInorder(struct node* root)
{
	while(root)
	{
		printf(" %d,", root->data);
		root = root->next;
	}
}  	

void main()
{
        struct node* tree = NULL;
        tree = insert(tree,4);
        insert(tree,2);
        insert(tree,3);
        insert(tree,1);
        insert(tree,6);
        insert(tree,7);
        insert(tree,5);

	populateInorder(tree);
	printInorder(tree->left->left);	
}
