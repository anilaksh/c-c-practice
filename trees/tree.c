#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "trees.h"

struct node* newNode(int data)
{
        struct node* newNode = (struct node*)malloc(sizeof(struct node));
        newNode->data = data;
        newNode->left = NULL;
        newNode->right = NULL;
        return newNode;
}

struct node* insert(struct node* root, int data)
{
        if(root == NULL)
        return newNode(data);
        if(data <= root->data)
                root->left = insert(root->left, data);
        else
                root->right = insert(root->right, data);
        return root;
}

int lookup(struct node *root, int data)
{
        if(root == NULL)
                return 0;
        if(root->data == data)
                return 1;
        if(data < root->data)
                return lookup(root->left, data);
        else
                return lookup(root->right, data);
}

void printTree(struct node* root)
{
        if(root == NULL)
                return;
        printTree(root->left);
        printf("%d,",root->data);
        printTree(root->right);
}

int size(struct node *root)
{
	if(root == NULL)
		return 0;
	else 
		return(size(root->left)+size(root->right)+1);
}

int maxdepth(struct node *root)
{
	int ldepth,rdepth;
	if(root == NULL)
		return 0;
	ldepth = maxdepth(root->left);
	rdepth = maxdepth(root->right);
	if(ldepth>rdepth)
		return(ldepth+1); 
	else
		return(rdepth+1);
}

int minValue(struct node* root)
{
	if(root == NULL)
		return -1;
	else if(root->left == NULL)
		return root->data;
	return minValue(root->left);
	
}

void printPostOrder(struct node* root)
{
	if(root == NULL)
		return;
	printPostOrder(root->left);
	printPostOrder(root->right);
	printf("%d,",root->data);
}

int hasSumPath(struct node* root, int sum)
{
	if(root == NULL)
		return(sum == 0);
	sum = sum - root->data;
	return(hasSumPath(root->left,sum)||hasSumPath(root->right,sum));
}

void printPathsUtil(int path[], int pathLen)
{
	int i;
	printf("\n");
	for(i=0;i<pathLen;i++)
	{
		printf("%d,", path[i]);
	}
}

void printPathsRecur(struct node* root, int path[], int pathLen)
{
	if(root == NULL)
		return;
	path[pathLen] = root->data;
	pathLen++;

	if(root->left == NULL && root->right == NULL)
	{
		printPathsUtil(path, pathLen);
		return;
	}
	else
	{
		printPathsRecur(root->left, path, pathLen);
		printPathsRecur(root->right, path, pathLen);
	}
} 

void printPaths(struct node* root)
{
	int path[100];
	printPathsRecur(root, path, 0);
}

void mirror(struct node* root)
{
	struct node* temp;
	if(root == NULL)
		return; 
	mirror(root->left);
	mirror(root->right);
	temp = root->left;
	root->left = root->right;
	root->right = temp;	
}

void doubleTree(struct node* root)
{
	struct node* temp;
	
	if(root == NULL)
		return;
	doubleTree(root->left);
	doubleTree(root->right);
	temp = root->left;
	root->left = newNode(root->data);
	root->left->left = temp;
}

int sameTree(struct node* a, struct node* b)
{
	if(a == NULL && b == NULL)
		return 1;
	else if(a != NULL && b != NULL)
	{
		return ((a->data == b->data) && sameTree(a->left,b->left) && sameTree(a->right,b->right));
	}
	else
		return 0;
}

int countTrees(int numKeys)
{
	int sum = 0;
	int left,right,root;
	if(numKeys <= 1)
		return 1;
	else
	{
		for(root=1;root<=numKeys;root++)
		{
			left = countTrees(root-1);
			right = countTrees(numKeys-root);
			sum += left*right;	
		} 
	}
}

void deleteTree(struct node* root)
{
	if(root == NULL)
		return;
	else
	{
		deleteTree(root->left);
		deleteTree(root->right);
		printf("\ndeleting node: %d", root->data);
		free(root);
	}
}

int sumLeafNodes(struct node* root)
{
	if(root == NULL)
		return 0;
	if(root->left == NULL && root->right == NULL)
		return 1;
	return(sumLeafNodes(root->left)+sumLeafNodes(root->right));
}

void printAtHeight(struct node* root, int height, int dir)
{
	if(root == NULL)
		return;
	if(height == 1)
		printf("%d,", root->data);
	else.
	{
		if(dir == 1)
		{
		printAtHeight(root->left, height-1, dir);
		printAtHeight(root->right, height-1, dir);
		}
		else
		{
		printAtHeight(root->right, height-1, dir);p0
		printAtHeight(root->left, height-1, dir);
		}
	}
}

void levelOrderTraversal(struct node* root)
{
	if(root == NULL)
		return;
	int h = maxdepth(root);

	int i,d;
	d = 1;
	for(i=1;i<=h;i++) 
	{
		printf("\nnodes at level %d: ",i);
		printAtHeight(root,i,d);	
		d = d*(-1);
	}
}

int verifyChildrenSumProperty(struct node* root)
{
	if(root == NULL)
		return 1;
	if(root->left == NULL && root->right == NULL)
		return 1;
	int lsum = 0;
	int rsum = 0;
	if(root->left != NULL)
		lsum = root->left->data;
	if(root->right !=NULL)
		rsum = root->right->data;
	return((root->data == lsum+rsum) && verifyChildrenSumProperty(root->left) &&  verifyChildrenSumProperty(root->right)); 
} 

void incrementTree(struct node* node, int diff)
{
	if(node->left != NULL)
	{	
		node->left->data += diff;
		incrementTree(node->left, diff);
	}	
	else if(node->right != NULL)
	{
		node->right->data += diff;
		incrementTree(node->right, diff);
	}	
}

void convertSumProperty(struct node* root)
{
	if(root == NULL)
		return;
	if(root->left == NULL && root->right == NULL)
		return;

	convertSumProperty(root->left);
	convertSumProperty(root->right);

	int lsum = 0;
	int rsum = 0;

	if(root->left != NULL)
		lsum = root->left->data; 
	if(root->right != NULL)
		rsum = root->right->data;
	int diff = (lsum+rsum) - root->data;

	if(diff > 0)
	{
		root->data += diff;
	}
	if(diff < 0)	
	{
		incrementTree(root, -diff);
	}
	
}

int max(int a, int b)
{
	if(a>b)
		return a;
	else
		return b;
} 

int diameter(struct node* root)
{

	if(root == NULL)
		return 0;
	int lheight = maxdepth(root->left);
	int rheight = maxdepth(root->right);

	int ldiameter = diameter(root->left);
	int rdiameter = diameter(root->right);

	return max(lheight+rheight+1, max(ldiameter,rdiameter));
}

int diameterOpt(struct node* root, int* height)
{
	if(root == NULL)
	{
		*height = 0;
		return 0;
	}
	int lh = 0;
	int rh = 0;
	
	int ldiameter = diameterOpt(root->left, &lh);
	int rdiameter = diameterOpt(root->right, &rh);

	*height = max(lh,rh) + 1;

	return max(lh+rh+1, max(ldiameter,rdiameter));
}

int isbalanced(struct node* root, int *height)
{
	if(root == NULL)
	{
		*height = 0;
		return 1;
	}

	int lh = 0;
	int rh = 0;

	int lbalanced = isbalanced(root->left, &lh);
	int rbalanced = isbalanced(root->right, &rh);

	*height = max(lh,rh)+1;
	
	return ((abs(lh-rh)<=1 ) && lbalanced && rbalanced);

	
}

int findElement(int data, int arr[], int start, int end)
{
	int i;
	for(i=start;i<=end;i++)
	{
		if(arr[i] == data)
			return i;
	}
}

struct node* constructTreeInAndPre(int pre[], int in[], int start, int end)
{
	static int preIndex=0;

	if(start>end)
		return NULL;
	
	struct node* node = newNode(pre[preIndex]);
	printf("\ninserting %d instat:%d inend:%d, preIn:%d",node->data,start,end,preIndex);
	
	preIndex++;
	if(start == end)
	{
		//printf("\ninserting at leaf %d",node->data);
		return node;
	}
	
	int i = findElement(node->data, in, start, end);

	node->left = constructTreeInAndPre(pre,in,start,i-1);
	node->right = constructTreeInAndPre(pre,in,i+1,end);

	//printf("\ninserting %d",node->data);

	return node;
} 

void countNodesAtLevel(struct node* root, int count[], int level)
{
	if(root)
	{
		count[level]++;
		countNodesAtLevel(root->left,count,level+1);
		countNodesAtLevel(root->right,count,level+1);
		
	}
} 

int maxWidth(struct node* root)
{
	int h = maxdepth(root);

	int* count = (int*)calloc(sizeof(int),h);
	
	countNodesAtLevel(root,count, 0);

	int maxwidth=0;
	int i;
	for(i=0;i<h;i++)
	{
		if(count[i]>maxwidth)
		maxwidth = count[i];
	}
	return maxwidth;	
}

int isFoldableUtil(struct node* node1, struct node* node2)
{
	if(node1 == NULL && node2 == NULL)
		return 1;
	if(node1 == NULL || node2 == NULL)
		return 0;

	return(isFoldableUtil(node1->left,node2->right) && isFoldableUtil(node2->left,node1->right));
	
}

int isFoldable(struct node* root)
{
	if(root == NULL)
		return 1;
	return isFoldableUtil(root->left, root->right);
}

void printKDistance(struct node* root, int k) 
{
	if(root)
	{
		if(k==0)
		{
			printf(" %d,",root->data);
			return;
		}
		printKDistance(root->left,k-1);
		printKDistance(root->right,k-1);	
	}
}

int getLevelUtil(struct node* root, int key, int level)
{
	if(root == NULL)
		return 0;
	if(root->data == key)
	{
		return level;
		
	}

	int nodeLevel = getLevelUtil(root->left, key, level+1);
	if(nodeLevel != 0)
		return nodeLevel;
	nodeLevel = getLevelUtil(root->right, key, level+1);
	if(nodeLevel != 0)
                return nodeLevel;
} 

int getLevel(struct node* root, int key)
{
	return getLevelUtil(root,key,1);
}

int printAncestors(struct node* root, int key)
{
	if(root == NULL)
		return 0;
	if(root->data == key)
	{
		return 1;
	}
	if(printAncestors(root->left,key) || printAncestors(root->right,key))
	{
		printf(" %d,", root->data);
		return 1;
	}
	
	return 0; 
	
}

int isSubTree(struct node* tree, struct node* sub)
{
	if(sub == NULL)
		return 1; 
	if(tree == NULL)
		return 0;
	return (sameTree(tree->left,sub) || sameTree(tree->right,sub));
}

int sumTree(struct node* root)
{
	int oldval;
	if(root == NULL)
		return 0;

	oldval = root->data;

	root->data = sumTree(root->left) + sumTree(root->right);

	return root->data + oldval;
}

int printMaxSumPath(struct node* root, struct node* leaf)
{
	if(root == NULL)
		return 0;
	if(root == leaf)
	{
		printf(" %d,", root->data);
		return 1;
	}
	if(printMaxSumPath(root->left, leaf) || printMaxSumPath(root->right, leaf))
	{
		printf(" %d,", root->data);
                return 1;
        }
	return 0;
}

void maxSumPathUtil(struct node* root, int sum, int* maxsum, struct node** leafnode)
{
	if(root == NULL)
		return;
	sum += root->data;

	if(root->left == NULL && root->right == NULL)
	{
		if(*maxsum < sum)
		{
			*maxsum = sum;	
			*leafnode = root;
		}
		return;
	}
	
	maxSumPathUtil(root->left, sum, maxsum, leafnode);	
	maxSumPathUtil(root->right, sum, maxsum, leafnode);
}

void maxSumPath(struct node* root)
{
	int maxsum = INT_MIN;
	struct node *maxsumleaf = NULL;

	maxSumPathUtil(root,0,&maxsum,&maxsumleaf);
	printMaxSumPath(root,maxsumleaf);
}

int maxElement(int inArray[], int st, int end)
{
	int i;
	int max = inArray[st];
	int maxIn = st;
	for(i=st+1;i<end;i++)
	{
		if(inArray[i] > max)
		{
			max = inArray[i];
			maxIn = i;
		}
	}
	return maxIn;
}

struct node* buildTreeSpecialInorder(int inArray[], int st, int end)
{
	if(st > end)
		return NULL;
	struct node* node = NULL;
	int i = maxElement(inArray, st, end);
	node = newNode(inArray[i]);
	if(st == end)
		return node;

	node->left = buildTreeSpecialInorder(inArray, st, i-1);
	node->right = buildTreeSpecialInorder(inArray, i+1, end);
	return node;
}

struct node* buildTreeSpecialPre(int pre[], char preLN[], int size)
{
	static int index = 0;	
	if(index > size)
		return NULL;
	struct node* node = NULL;
	node = newNode(pre[index]);

	if(index == size)
		return node;
	index++;

	if(preLN[index-1] == 'N')
	{
		node->left = buildTreeSpecialPre(pre, preLN, size);
		node->right = buildTreeSpecialPre(pre, preLN, size);
	}
	return node;
	
}

int isCompleteTree(struct node* root)
{
	if(root == NULL)
		return 1;
	if(root->left == NULL && root->right == NULL)
		return 1;
	if(root->left == NULL || root->right == NULL)
		return 0;
	return(isCompleteTree(root->left) && isCompleteTree(root->right));
}

void printLeftBoundary(struct node* root)
{
	if(root == NULL)
		return;
	if(root->left == NULL && root->right == NULL)
		return;
	printf(" %d,", root->data);
	if(root->left)
                 printLeftBoundary(root->left);
	else
		 printLeftBoundary(root->right);
}

void printLeafNodes(struct node* root)
{
	if(root == NULL)
		return;
	if(root->left == NULL && root->right == NULL)
		printf(" %d,", root->data);
	printLeafNodes(root->left);
	printLeafNodes(root->right);
}

void printRightBoundary(struct node* root)
{
	if(root == NULL)
		return;
	if(root->left == NULL && root->right == NULL)
		return;
	if(root->right)
		printRightBoundary(root->right);
	else
		printRightBoundary(root->left);
	printf(" %d,", root->data);
}

void boundaryTraversal(struct node* root)
{
	if(root == NULL)
		return;
	printf(" %d,", root->data);
	printLeftBoundary(root->left);
	printLeafNodes(root->left);
	printLeafNodes(root->right);
	printRightBoundary(root->right);
}

void printPostOrderFromPreIn(int pre[], int in[], int st, int end)
{
	static int preIndex = 0;

	if(st > end)
                return;
        int index = findElement(pre[preIndex], in, st, end);
	preIndex++;
	
        printPostOrderFromPreIn(pre,in,st,index-1);
        printPostOrderFromPreIn(pre,in,index+1,end);

        printf(" %d,",in[index]);

}

void leftviewUtil(struct node* root, int level, int *maxlevel)
{
	if(root == NULL)
		return;
	if(*maxlevel < level)
	{
		printf(" %d,", root->data);
		*maxlevel = level;
	}
	leftviewUtil(root->left,level+1,maxlevel);
	leftviewUtil(root->right,level+1,maxlevel);
}

void leftview(struct node *root)
{
	int maxlevel = 0; 
	int level = 1;
	leftviewUtil(root, level, &maxlevel);
}

void convertTreeToDLLUtil(struct node* root, struct node** prev, struct node** head_rf)
{
	if(root == NULL)
		return;

	convertTreeToDLLUtil(root->left, prev, head_rf);

	if(*prev == NULL)		
		*head_rf = root;
	else
	{
		root->left = *prev;
		(*prev)->right = root;
	}
	*prev = root;
	convertTreeToDLLUtil(root->right, prev, head_rf);
}

struct node* convertTreeToDLL(struct node* root)
{
	struct node* prev = NULL;
	struct node* head = NULL; 
	convertTreeToDLLUtil(root, &prev, &head);
	return head;
}

int maxOddLevelUtil(struct node* root, int level)
{
	if(root == NULL)
		return 0;
	if(root->left == NULL && root->right == NULL && level&1)
		return level;
	return max(maxOddLevelUtil(root->left,level+1),
	maxOddLevelUtil(root->right,level+1));
}

int maxOddLevel(struct node* root)
{
	return maxOddLevelUtil(root, 1); 
} 

isLeavesAtSameLevelUtil(struct node* root,int level, int *leafLevel)
{
	if(root == NULL)
		return 1;
	if(root->left == NULL && root->right == NULL)
	{
		if(*leafLevel == 0)
		{
			*leafLevel = level;
			return 1;
		}
		else if(*leafLevel == level)
			return 1;
		else
			return 0;
	}
	return(isLeavesAtSameLevelUtil(root->left, level+1, leafLevel) && isLeavesAtSameLevelUtil(root->right, level+1, leafLevel));
	
}

int isLeavesAtSameLevel(struct node* root)
{
	int leafLevel = 0;
	int level =1;
	return isLeavesAtSameLevelUtil(root, level, &leafLevel);
}

struct node* removeNodesUtil(struct node* root, int k, int *sum)
{
	if(root == NULL)
		return NULL;

	int lsum = root->data + (*sum);
	int rsum = lsum;
	root->left = removeNodesUtil(root->left, k, &lsum);
	root->right = removeNodesUtil(root->right,k, &rsum);
	
	*sum = max(lsum,rsum);
	if(*sum < k)
	{
		free(root);
		root = NULL;
	}
	return root;
} 

struct node* removeNodes(struct node* root, int k)
{
	int sum = 0;
	return removeNodesUtil(root,k,&sum);
}

v leafNodesToDLLUtil(struct node* root, struct node** head_rf, struct node** prev)
{
	if(root == NULL)
		return;

	leafNodesToDLLUtil(root->left,head_rf,prev);
	
	if(root->left == NULL && root->right == NULL)
	{
		if(*prev == NULL)
		{
			*head_rf = root;
			
		}
		else
		{
			root->left = *prev;
			(*prev)->right = root;
			
		}
		*prev = root;
		printf(" %d", root->data);
	}


	leafNodesToDLLUtil(root->right,head_rf,prev);
	
}

struct node* leafNodesToDLL(struct node* tree)
{
	struct node* head_rf = NULL;
	struct node* prev = NULL;
	leafNodesToDLLUtil(tree, &head_rf, &prev);
	return head_rf;	
}  

int findLCA(struct node* root, int n1, int n2)
{
}

void buildFullTreePrePostUtil(struct node* root, int pre[], int post[], int *preIndex, int postSt, int postEnd)
{
	if(*preIndex < postEnd)
	{
		root->left = newNode(pre[(*preIndex)]);
	}
}

struct node* buildFullTreePrePost(int pre[], int post[], int size)
{
	int preIndex = 0;
	struct node* root = NULL;
	root = newNode(pre[preIndex]);
	preIndex++;
	buildFullTreePrePostUtil(root, pre, post, &preIndex, postSt, postEnd); 
}

void main()
{
	struct node* a = NULL;
	struct node* b = NULL; 
	struct node* tree = NULL;
	tree = insert(tree,4);
	insert(tree,2);
	insert(tree,3);
	insert(tree,1);
	insert(tree,6);
        insert(tree,7);
        insert(tree,5);
	insert(tree,10);

	a = newNode(2);	
	a->left = newNode(1);
        a->right = newNode(3);

	b = insert(b, 2);	
        insert(b, 3);
	insert(b, 1);
	
	printTree(tree); 
	
	printf("\n\n");
	printf("size of tree: %d", size(tree)); 

	printf("\n\n");
	printf("maxdepth of tree: %d", maxdepth(tree));

	printf("\n\n");
	printf("minValue of tree: %d", minValue(tree));

	printf("\n\nPost Order Traversal: ");
	printPostOrder(tree);

	printf("\n\n");
	printf("HasSumPath: %d,%d", hasSumPath(tree,9), hasSumPath(tree,100)); 

	printf("\n\nprintPaths: ");
	printPaths(tree);

/*	printf("\n\nMirror: ");
	mirror(tree);
	printTree(tree);

	printf("\n\nMirror: ");
        mirror(tree);
        printTree(tree);

	printf("\n\nDoubleTree: ");
	doubleTree(tree);
	printTree(tree); 

	printf("\n\n");
	printf("sameTree: %d", sameTree(a,b)); 

	printf("\n\n");
	printf("delete tree");
	deleteTree(tree); 

	printf("\n\n");
	printf("sum of left nodes: %d", sumLeafNodes(tree));

	printf("\n\n");
	printf("level order traversal: ");
	levelOrderTraversal(tree);

	printf("\n\n");
	printf("isSumTree: %d", verifyChildrenSumProperty(tree));

	printf("\n\n");
	printf("convert to sum property:");
	convertSumProperty(tree);
	printTree(tree);
	
	printf("\n\n");
        printf("isSumTree: %d", verifyChildrenSumProperty(tree)); 

	printf("\n\n");
	printf("Diameter: %d", diameter(tree));
	
	int height = 0; 
	printf("\n\n");
	printf("is height balanced: %d", isbalanced(tree,&height)); 

	int InArray[7] = {4,2,5,1,6,3,7};
	int preArray[7] = {1,2,4,5,3,6,7}; 
	printf("\n\n");
	printf("construct tree from inorder and preorder traversals:");
	struct node *c = NULL;
	c = constructTreeInAndPre(preArray,InArray,0,6);
	printTree(c);

	printf("\n\n");
	printf("maxwidth: %d", maxWidth(tree));

	printf("\n\n");
	printf("isFoldable: %d", isFoldable(tree));

	printf("\n\n");
	printf("printKDistance: ");
	printKDistance(tree,1); 

	printf("\n\n");
	printf("getLevel of %d: %d",3, getLevel(tree,7));

	printf("\n\n");
	printf("printAncestors: ");
	printAncestors(tree,7);

	if(isSubTree(tree,a))
	{
		printf("\n\n");
		printf("a is subtree of tree");
	}

	printf("\n\n");
	printf("sumTree");
	sumTree(tree);
	printTree(tree); 

	printf("\n\n");
	printf("maxSumPathValue: ");
	maxSumPath(tree); 

	int inArray[] = {5,10,40,30,20};
	struct node *intree = NULL;
	printf("\n\n");
	printf("Special Inorder tree: ");
	intree = buildTreeSpecialInorder(inArray, 0, 4);
	printTree(intree); 

	printf("\n\n");
	int pre[] = {10, 30, 20, 5, 15};
	char preLN[] = {'N', 'N', 'L', 'L', 'L'};
	struct node* pretree = NULL;
	printf("\n\n");
        printf("Special preorder tree: ");
        pretree = buildTreeSpecialPre(pre, preLN, 4);
        printTree(pretree);  

	printf("\n\n");
	printf("isCompleTree: %d", isCompleteTree(tree)); 

	printf("\n\n");
	printf("Boundary Traversal: ");
	boundaryTraversal(tree);

	printf("\n\n");
	int in[] = {4, 2, 5, 1, 3, 6};
	int pre[] = {1, 2, 4, 5, 3, 6};
	printf("print post order given inorder and preorder: ");
	printPostOrderFromPreIn(pre,in,0,5); 

	printf("\n\n");
	printf("left view: ");
	leftview(tree); 

	printf("\n\n");
	struct node *head = NULL; 
	head = convertTreeToDLL(tree);
	printf("coverted to DLL: "); 
	while(head->right != NULL)
	{
		printf(" %d,",head->data);
		head = head->right;
	}
	while(head->left != NULL)
	{
		printf(" %d,",head->data);
		head = head->left;
	} 
	tree = removeNodes(tree,10);
	printf("\n\n");
	printf("remove nodes with roof to sum path < k: ");
	printTree(tree); */

	printf("\n\n");
	struct node *head = NULL; 
	head = leafNodesToDLL(tree);
	printf("leaf nodes to DLL: "); 
	/*if(head)
	{
	while(head->right != NULL)
	{
		printf(" %d,",head->data);
		head = head->right;
	}
	printf(" %d,",head->data);
	while(head->left != NULL)
	{
		printf(" %d,",head->data);
		head = head->left;
	} 
	printf(" %d,",head->data);
	}*/

	
}
