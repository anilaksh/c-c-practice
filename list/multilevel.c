#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct node
{
	int data;
	struct node* right;
	struct node* down;
};

struct node* newNode(int data)
{
	struct node* new_node = (struct node*)malloc(sizeof(struct node));
	new_node->data = data;
	new_node->right = NULL;
	new_node->down = NULL;
}

void printList(struct node* head)
{
	while(head)
	{
		printf(" %d,",head->data);
		head = head->down;
	}
}

void printListRight(struct node* head)
{
	while(head)
	{
		printf("%d ,", head->data);
		head = head->right;
	}
}

struct node* merge(struct node* a, struct node* b)
{
	if(a == NULL)
		return b;
	if(b == NULL)
		return a;
	struct node* result = NULL;
	if(a->data < b->data)
	{
		result = newNode(a->data);
		result->down = (a->down, b); 
	}
	else
	{
		result = newNode(b->data);
		b = b->down;	
	}
	return result;
}

struct node* flatten(struct node* head)
{
	if(head == NULL || head->right == NULL)
		return head;
	return merge(head, flatten(head->right));
}

void push (struct node** head_ref, int data)
{
    struct node* new_node = newNode(data); 
 
    new_node->down = (*head_ref);
 
    (*head_ref) = new_node;
}

struct node* getTail(struct node* head)
{
	printf("*****");
	struct node* curr = head;
	if(curr == NULL)
		return NULL;

	while(curr->right)	
		curr = curr->right;
	printf("**%d***",curr->data);
	return curr;
}

struct node* flattenNew(struct node* head)
{
	if(head == NULL || head->right == NULL)
		return head;

	struct node* curr = head;
	struct node* tail = getTail(head);
	struct node* tmp;
	printf("\n%d %d", head->data, tail->data); 

	while(curr)
	{
		tmp = curr->down;
		if(tmp)
			tail->right = tmp;
		while(tmp->right)
			tmp = tmp->right;
		tail = tmp;
		curr = curr->right;
	}
	return head;
}

void main()
{
	struct node* root = NULL;
 
    /* Let us create the following linked list
       5 -> 10 -> 19 -> 28
       |    |     |     |
       V    V     V     V
       7    20    22    35
       |          |     |
       V          V     V
       8          50    40
       |                |
       V                V
       30               45
    */
    push( &root, 30 );
    push( &root, 8 );
    push( &root, 7 );
    push( &root, 5 );
 
    push( &( root->right ), 20 );
    push( &( root->right ), 10 );
 
    push( &( root->right->right ), 50 );
    push( &( root->right->right ), 22 );
    push( &( root->right->right ), 19 );
 
    push( &( root->right->right->right ), 45 );
    push( &( root->right->right->right ), 40 );
    push( &( root->right->right->right ), 35 );
    push( &( root->right->right->right ), 20 );

	struct node* head = flattenNew(root);
	printListRight(head);
}
