#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>

struct node
{
	int data;
	struct node *next;
};

typedef struct node* lnode;

struct node* newNode(int data)
{
	struct node* new_node = NULL;
	new_node = (struct node*)malloc(sizeof(struct node));
	new_node->data = data;
	new_node->next = NULL;
	return new_node;
}

void insert(struct node** head_ref, int data)
{
	struct node* new_node = newNode(data); 
	
	new_node->next = (*head_ref);
	(*head_ref) = new_node;

}

void delete(struct node** head_ref, int key)
{
	struct node* curr = (*head_ref);
	struct node* prev = NULL;

	if( curr!= NULL && curr->data == key)
	{
		(*head_ref) = curr->next;
		free(curr);
		return;
	}
	else
	{
		while(curr != NULL && curr->data != key)
		{  
			prev = curr;
			curr = curr->next; 
		}
		if(curr == NULL) return;
		
		prev->next = curr->next; 
		free(curr);
	}
	
}

void printList(struct node* head)
{
	while(head != NULL)
	{
		printf(" %d,", head->data);
		head = head->next;
	}	
}

int getLength(struct node* head)
{
	if(head == NULL)
		return 0;
	int count = 0;
	while(head != NULL)
	{
		count++;
		head = head->next;
	}
	return count;
}

int getLengthRecur(struct node* head)
{
	if(head == NULL)
		return 0;
	return(1+getLengthRecur(head->next));
} 

int getMiddle(struct node* head)
{
	struct node* slow = head;
	struct node* fast = head->next; 
	
	if(head == NULL)
		return;

	while(fast != NULL && fast->next != NULL)
	{
		slow = slow->next;
		fast = fast->next->next;
	}
	return slow->data;
}


void getMiddleNode(struct node* head, struct node** mid, struct node** prev_mid)
{
	struct node* slow = head;
	struct node* fast = head->next; 
	
	if(head == NULL)
		return;

	while(fast != NULL && fast->next != NULL)
	{
		*prev_mid = slow;
		slow = slow->next;
		fast = fast->next->next;
	}
	*mid = slow;
}

void nNodeFromEnd(struct node* head, int n)
{
	struct node* first = head;
	struct node* second = head;

	if(head == NULL)
		return;	

	int index = 0;
	while( index < n)
	{
		if(first == NULL)
			return;
		index++;
		first = first->next;
	}
	while(first != NULL)
	{
		second = second->next;
		first = first->next;
	}
	printf("\n\n n node from end is:%d", second->data);	
}

void deleteList(struct node** head)
{
	struct node* curr = *head;
	struct node* temp;
	while(curr != NULL)
	{
		temp = curr;
		curr = curr->next;
		free(temp);
	}
	*head = NULL;
}

int countOccurance(struct node* head, int key)
{
	int count = 0;
	while(head != NULL)
	{
		if(head->data == key)
			count++;
		head = head->next;
	}
	return count;
}

void reverseList(struct node** head)
{
	struct node* p = NULL;
	struct node* q = NULL;
	struct node* r = NULL;

	q = (*head);

	while(q != NULL)
	{
		r = q->next;
		q->next = p;
		p = q;
		q = r;
		
	}
	*head = p;
}

void reverseListRecurUtil(struct node** head, struct node* curr)
{
	if(curr == NULL)
		return;
	if(curr->next == NULL)
	{
		*head = curr;
		return;
	}
	reverseListRecurUtil(head,curr->next);
	curr->next->next = curr;
	curr->next = NULL;
} 

void reverseListRecur(struct node** head)
{
	struct node* curr = *head;
	reverseListRecurUtil(head,curr);
}

int detectLoop(struct node* head)
{
	if(head == NULL)
		return 0;

	struct node* slow = head;
	struct node* fast = head;

	while(fast != NULL && fast->next != NULL)
	{
		slow = slow->next;
		fast = fast->next->next;
		if(slow == fast)
			return 1;
	}

	return 0;
}

int compareList(struct node* temp1, struct node* temp2)
{
	while(temp1 && temp2)
	{
		if(temp1->data != temp2->data)
			return 0;
		temp1 = temp1->next;
		temp2 = temp2->next;
	}
	if(temp1 || temp2)
		return 0;
	return 1;
}

int compareListRec(struct node* temp1, struct node* temp2)
{
	if(temp1 == NULL && temp2 == NULL)
		return 1;
	if((temp1 && !temp2) || (temp2 && !temp1))
		return 0;
	return(temp1->data == temp2->data && compareListRec(temp1->next,temp2->next)); 
}

void removeDuplicates(struct node* head)
{
	struct node* temp;

	while(head && head->next)
	{
		if(head->data == head->next->data)
		{
			temp = head->next;
			head->next = head->next->next;
			free(temp);
		}
		else
		{
			head = head->next;
		}
	}	
}

void removeDuplicates1(struct node* head)
{
	struct node *nextNode, *prev, *temp;
	
	while(head)
	{
		prev = head;
		nextNode = head->next;
		while(nextNode)
		{
			if(head->data == nextNode->data)
			{
				temp = nextNode;
				prev->next = nextNode->next;
				free(temp);
			}
			else
			{
				prev = prev->next;
			}	
			nextNode = prev->next;
		}
		head = head->next;	
	}
}

int isPalindrome(struct node* head)
{
	if(head == NULL || head->next == NULL)
		return 1;

	int res = 0;
	int count = getLength(head);

	struct node* mid = NULL; 
	struct node* prevToMid = NULL;
	getMiddleNode(head, &mid, &prevToMid);

	
	struct node* first = head;
	struct node* sec = mid->next;
	mid->next = NULL;
	reverseList(&sec);

	if(count%2 == 0)
	{
		res = compareList(first,sec);
		printf("\n\n");
		printList(first);
		printf("\n\n");
		printList(sec);
		reverseList(&sec);
		mid->next = sec;
	}
	else
	{
		prevToMid->next = NULL;
                res = compareList(first,sec);
		reverseList(&sec);
		prevToMid->next = mid;
		mid->next = sec;
	}	
	return res;
}

void insertInsortedList(struct node** head, int key)
{
	struct node* new_node = newNode(key);
	if((*head) == NULL || key < (*head)->data)
	{
		*head = new_node;**
		return;
	}
	struct node* curr = (*head);
	while(curr && curr->next && key > curr->next->data)
	{
		curr = curr->next;
	}
	new_node->next = curr->next;
	curr->next = new_node; 
} 

struct node* reverseKNodes(struct node* head, int k)
{
	struct node* curr = head;
	struct node* prev = NULL;
	struct node* next = NULL;
	if(head == NULL)
		return NULL;
	
	int index = 0;

	while(curr && index < k)
	{
		next = curr->next;
		curr->next = prev;
		prev = curr;
		curr = next;
		index++;
	}

	head->next = reverseKNodes(curr, k); 
	return prev;
}

struct node* reverseAltKNodes(struct node* head, int k)
{

	struct node* curr = head;
	struct node* prev = NULL;
	struct node* next = NULL;
	if(head == NULL)
		return NULL;
	
	int index = 0;

	while(curr && index < k)
	{
		next = curr->next;
		curr->next = prev;
		prev = curr;
		curr = next; 
		index++;
	}

	head->next = curr;
	index = 0;

	while(curr && index < k-1)
	{
		curr = curr->next;
		index++;
	}

	if(curr)
	{
		curr->next = reverseAltKNodes(curr->next, k);
	}
	return prev;
}

void swap(int* a, int* b)
{
	int temp = (*a);
	*a = *b;
	*b = temp;
}

void pairSwapRec(struct node* head)
{
	if(head == NULL || head->next == NULL)
		return;

	swap(&(head->data), &(head->next->data));

	pairSwapRec(head->next->next);
}

void pairSwap(struct node* head)
{
	while(head && head->next)
	{
		swap(&(head->data), &(head->next->data));
		head = head->next->next;
	}
}

void deleteSpecial(struct node** head)
{
	if((*head) == NULL)
		return;

	reverseList(head);

	struct node* curr = (*head);
	struct node* temp;
	
	int max = curr->data;
	while(curr->next != NULL)
	{
		temp = curr->next;
		if(temp->data >= max)
		{
			max = temp->data;
			curr = curr->next;
		}	
		else
		{
			curr->next = curr->next->next;
			free(temp);
		}
	}

	reverseList(head); 
}

void segregateEvenOdd(struct node** head)
{
	if(*head == NULL || (*head)->next == NULL)
		return;

	struct node* tail = *head;	
	struct node* orig_tail = NULL;
	struct node* temp = NULL;

	while(tail && tail->next)
		tail = tail->next;

	orig_tail = tail;	

	while((*head) && (*head)->data%2 != 0)
	{ 
		temp = *head;
		*head = (*head)->next;
		tail->next = temp;
		tail = tail->next;
		tail->next = NULL;
	}

	struct node* curr = *head;

	while(tail != orig_tail)
	{
		temp = curr->next;
		if(temp->data%2 != 0)
		{
			curr->next = temp->next;
			tail->next = temp;
			tail = tail->next;
			tail->next = NULL;
		}
		curr = curr->next;
		printf("\n\n");
		printList(*head);
	}

}

void push(struct node** head, int data)
{

	struct node* new_node = newNode(data);
	new_node->next = *head;
	*head = new_node;
}

struct node* intersection(struct node* a, struct node* b)
{
	struct node dummy;
	struct node* tail = &dummy;
	dummy.next = NULL;

	while(a && b)
	{
		if(a->data == b->data)
		{
			push(&(tail->next), a->data);
			tail = tail->next;
			a = a->next;
			b = b->next;
		}
		else if(a->data < b->data)
			a = a->next;
		else
			b = b->next;	
	}
	return dummy.next;
}

struct node* unionList(struct node* a, struct node* b)
{
	struct node dummy;
	struct node* tail = &dummy; 
	dummy.next = NULL;

	while(a && b)
	{
		if(a->data == b->data)
		{
			push(&(tail->next), a->data);
                        tail = tail->next;
			a = a->next;
			b = b->next;
		}
		else if(a->data < b->data)
		{
			push(&(tail->next), a->data);
			tail = tail->next;	
			a = a->next;
		}
		else 
		{
			push(&(tail->next), b->data);
                        tail = tail->next;
                        b = b->next;
		}
	}
	while(a)
	{

		push(&(tail->next), a->data);
		tail = tail->next;	
		a = a->next;
	}
	while(b)
	{
		push(&(tail->next), b->data);
                tail = tail->next;
                b = b->next;
	}
	return dummy.next;
}

void alternativeSplit(struct node* head, struct node** a, struct node** b)
{
	struct node dummy1;
	struct node dummy2;
	struct node* tail1 = &dummy1;
	struct node* tail2 = &dummy2;
	struct node* curr = head;
	
	dummy1.next = NULL;
	dummy2.next = NULL; 

	while(curr)
	{
		push(&(tail1->next), curr->data);
		tail1 = tail1->next;
		curr = curr->next;
		if(curr)
		{
			push(&(tail2->next), curr->data);
			tail2 = tail2->next;
			curr = curr->next;
		}

	}
	*a = dummy1.next;
	*b = dummy2.next;
}

struct node* merge(struct node* a, struct node* b)
{
	struct node dummy;
	struct node* tail = &dummy;
	dummy.next = NULL; 

	while(a && b)
	{
		if(a->data < b->data)
		{
			push(&(tail->next), a->data);
			tail = tail->next;
			a = a->next;
		}
		else
		{
			push(&(tail->next), b->data);
			tail = tail->next;
			b = b->next;
		}
	}
	while(a)
	{
		push(&(tail->next), a->data);
                tail = tail->next;
                a = a->next;
	}
	while(b)
	{
		push(&(tail->next), b->data);
                tail = tail->next;
                b = b->next;

	}
	return dummy.next;	
}

struct node* mergeRecur(struct node* a, struct node* b)
{
	struct node* result = NULL;

	if(a == NULL)
		return b;

	if(b == NULL)
		return a;

	if(a->data < b->data)
	{
		result = newNode(a->data);
		result->next = mergeRecur(a->next, b); 
	}
	else
	{
		result = newNode(b->data);
		result->next = mergeRecur(a, b->next);
	}

	return result;
}

void frontBackSplit(struct node* head, struct node** a, struct node** b)
{
	if(head == NULL)
		return;

	struct node* slow = head;
	struct node* fast = head->next; 
	while(fast && fast->next)
	{
		slow = slow->next;
		fast = fast->next->next;	
	}
	*a = head;
	*b = slow->next;
	slow->next = NULL;
}

void mergeSort(struct node** headRef)
{
	if(*headRef == NULL || (*headRef)->next == NULL)
		return;
	struct node* a;
	struct node* b;
	frontBackSplit(*headRef, &a, &b);
	mergeSort(&a);
	mergeSort(&b);
	*headRef = merge(a,b);
}

void rotate(struct node** headRef, int k)
{
	struct node* head = *headRef;
	struct node* curr = *headRef;
	struct node* kthNode;
	
	
	int index = 1;
	while(curr && index < k)
	{
		curr = curr->next;
		index++;
	}
	kthNode = curr;
	while(curr && curr->next)
		curr = curr->next;

	if(kthNode)
	{
		*headRef = kthNode->next;
		kthNode->next = NULL;
		curr->next = head; 
	}
}

void skipMdeleteN(struct node* head, int m, int n)
{
	struct node* curr = head;
	int index;
	struct node *mthNode,*temp;
	while(curr)
	{
		for(index = 1;curr && index<m;index++)
			curr = curr->next;
		if( curr == NULL)
			return;
		mthNode = curr;
		curr = curr->next;
		for(index=1;curr&&index<=n;index++)
		{
			temp = curr;
			curr = curr->next;
			free(temp);
		}
		mthNode->next = curr;
	}
}

struct node* sumList1(struct node* head1, struct node* head2)
{
	int carry = 0;
	int tempsum = 0;
	struct node dummy;
	struct node* result = &dummy;
	dummy.next = NULL;
	
	while( head1 && head2)
	{
		tempsum = head1->data + head2->data + carry;
		carry = tempsum/10; 	
		push(&(result->next), tempsum%10);
		result = result->next; 
		head1 = head1->next;
		head2 = head2->next;
	}
	while(head1)
	{
		tempsum = head1->data + carry;
                carry = tempsum/10;
		push(&(result->next), tempsum%10);
                result = result->next;
                head1 = head1->next;
	}
	while(head2)
	{
		tempsum = head2->data + carry;
                carry = tempsum/10;
		push (&(result->next), tempsum % 10);
		result = result->next;
                head2 = head2->next;
	}
	if(carry)
	{
		push(&(result->next), carry);
                result = result->next;
	}	
	return dummy.next;
}

struct node* sumListUtil(struct node* head1, struct node* head2, int* carry)
{
	if(head1 == NULL || head2 == NULL)
		return NULL; 
	struct node* next = sumListUtil(head1->next, head2->next, carry);
	struct node* result;
	int tempSum = head1->data + head2->data + (*carry);
	*carry = tempSum/10;
	result = newNode(tempSum%10);
	result->next = next;
	return result;
}	

void addCarryToRemaining(struct node* head, struct node* temp, int* carry, struct node** result)
{
	if(head == temp)
		return; 
	addCarryToRemaining(head->next, temp, carry, result);
	int sum = head->data + (*carry);
	*carry = sum/10;
	sum = sum%10;
	push(result, sum);
}	

struct node* sumList2(struct node* head1, struct node* head2)
{
	if(head1 == NULL)
		return head2;
	if(head2 == NULL)
		return head1;

	int n1 = getLength(head1);
	int n2 = getLength(head2);
	int i;
	int carry = 0;
	struct node *result,*temp;

	if(n1 == n2)
		return sumListUtil(head1, head2, &carry);
	else if( n1 > n2)
	{
		temp = head1;
		int diff = n1-n2; 
		for(i=0;i<diff;i++)
			temp = temp->next;
		result = sumListUtil(temp, head2, &carry);
		addCarryToRemaining(head1, temp, &carry, &result);
	}
	else
	{
		temp = head2;
		int diff = n2-n1;
		for(i=0;i<diff;i++)
			temp = temp->next;
		result = sumListUtil(head1, temp, &carry);
		addCarryToRemaining(head2, temp, &carry, &result);
	}
	return result;
}

struct node* getTail(struct node* head)
{
	if(head == NULL)
		return NULL;
	while(head && head->next)
		head = head->next;
	return head;	
}
	
struct node* partition(struct node* head, struct node* end, struct node** headNew, struct node** endNew)
{
	if(head == NULL)
		return NULL;

	struct node* pivot = end;
	
	struct node* prev = NULL;
	struct node* temp;
	
	while(head!=pivot)
	{
		if(head->data < pivot->data)
		{
			if(*headNew == NULL)
				*headNew = head;
			prev = head;
			head = head->next;
		}
		else
		{
			if(prev)
				prev->next = head->next;
			temp = head;
			head = head->next;
			end->next = temp;
			end = end->next;
			end->next = NULL;
		}
	}
	if(*headNew == NULL)
		*headNew = pivot; 
	*endNew = end;
	return pivot;
}

struct node* quickSortRecur(struct node* head, struct node* end)
{
	if(head == NULL || head == end)
		return head;

	struct node* headNew = NULL;
	struct node* endNew = NULL;
	struct node* pivot = partition(head, end, &headNew, &endNew);
	
	struct node* curr = headNew;
	if(headNew!= pivot)
	{
	while(curr && curr->next != pivot)
		curr = curr->next;
	curr->next = NULL;
	headNew = quickSortRecur(headNew, curr);
	
	curr = getTail(headNew);
	curr->next = pivot;
	}
	
	pivot->next = quickSortRecur(pivot->next, endNew);
	return headNew; 
}

void quickSort(struct node** headref)
{
	*headref = quickSortRecur(*headref, getTail(*headref));
	return;
}

void mergeAlternative(struct node* a, struct node** b)
{
	if(a == NULL)
		return;
	
	struct node* a_curr = a;
	struct node* b_curr = (*b);
	struct node* a_next = NULL;
	struct node* b_next = NULL;
	
	while(a_curr && b_curr)
	{
		a_next = a_curr->next;
		b_next = b_curr->next;
	
		a_curr->next = b_curr;
		b_curr->next = a_next;

		a_curr = a_next;
		b_curr = b_next;
	}
	*b = b_curr;
}

void pairWiseSwap(struct node** head)
{
	if((*head) == NULL || (*head)->next == NULL)
		return; 

	struct node* prev = NULL;
	struct node* curr = (*head);
	struct node* next = curr->next;

	(*head) = next;	

	while(curr && next)
	{
		curr->next = next->next;
		next->next = curr;
		if(prev)
			prev->next = next;
		prev = curr;
		curr = curr->next;
		if(curr)
			next = curr->next;
	}
}

struct node* pairWiseSwapRecur(struct node* head)
{
	if(head == NULL || head->next == NULL)
		return head;

	struct node* temp = head->next;
	head->next = pairWiseSwapRecur(head->next->next);
	if(temp)
		temp->next = head;
	return temp;
}

void main()
{
	bool test = true;	
	struct node* head = NULL;
	insert(&head, 6);
	insert(&head, 4);
	insert(&head, 9);
	insert(&head, 5);
	insert(&head, 7);
	printList(head);

	struct node* head1 = NULL;
	insert(&head1, 10);
	insert(&head1, 99);
	insert(&head1, 19);
	insert(&head1, 23);
	insert(&head1, 45);
	insert(&head1, 8);

	struct node* final = NULL;
	struct node *a, *b;

	printf("\n\n");
	printList(head1);

	printf("\n\nlegth of the linked list is:%d", getLength(head));
	printf("\n\nlegth of the linked list is:%d", getLengthRecur(head));
	printf("\n\nmiddle element of the ll is:%d", getMiddle(head));
	
	/*deleteList(&head);
	if(head == NULL)
		printf("\n\nlist deleted");*/
	//printf("\n\nisPalindrome:%d", isPalindrome(head));
	//printf("\n\nCompare list: %d", compareList(head,head1));
	//printf("\n\nCompare list: %d", compareListRec(head,head1));
	//insertInsortedList(&head,35);
        //removeDuplicates1(head);
	//head1 = reverseAltKNodes(head1,3);
	//reverseListRecur(&head);
	//pairSwap(head1);
	//deleteSpecial(&head1);
	//segregateEvenOdd(&head1);
	//final = unionList(head, head1);
	//final = intersection(head, head1);
	
	/*alternativeSplit(head1, &a, &b);
	printf("\n\n");
	printList(a);
	printf("\n\n");
	printList(b);*/

	//final = mergeRecur(head, head1);
	//mergeSort(&head1);
	//rotate(&head1, 4);
	//skipMdeleteN(head1,1,2);
	//quickSort(&head1); 
	//mergeAlternative(head, &head1);
	head1 = pairWiseSwapRecur(head1);
	head = pairWiseSwapRecur(head);
	printf("\n\n");
	printList(head);	
	printf("\n\n");
	printList(head1);
	printf("\n\n");
}	
