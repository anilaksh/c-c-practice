#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

struct node{
int key;
struct node* next;
};

struct node* newNode(int key)
{
	struct node* new_node = (struct node*)malloc(sizeof(struct node));
	new_node->key = key;
	new_node->next = NULL;
	return new_node;
}

void push(struct node** top, int key)
{
	struct node* new_node = newNode(key);
	new_node->next = (*top);
	*top = new_node;	
	printf("\nPUSH pushed element %d", key);
}

int isEmpty(struct node* top)
{
	if(top == NULL)
		return 1;
	return 0;
}

int pop(struct node** top)
{
	if(isEmpty(*top))
	{
		printf("\nPOP the stack is empty");
		return INT_MIN;
	}

	struct node* old_top = (*top);
	int popped = (*top)->key;
	(*top) = (*top)->next;
	printf("\nPOP popped element of the stack is %d", popped); 
	free(old_top);
	return popped;	
}

int peek(struct node* top)
{
	if(isEmpty(top))
	{
		printf("\nPEEK the stack is empty");
		return INT_MIN;
	}
	else
	{
		printf("\nPEEK the top element of the stack is %d", top->key); 
		return top->key;	
	}
	
}
 
void main()
{

	struct node* top = NULL;
	push(&top, 3);
	push(&top, 2);
	push(&top, 1);
	peek(top);
	pop(&top);
	peek(top);
	pop(&top);
	peek(top);
	pop(&top);
	peek(top);

}
