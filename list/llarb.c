#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

struct node
{
	int data;
	struct node* next;
	struct node* arb;
};

struct node* newNode(int data)
{
	struct node* new_node = (struct node*)malloc(sizeof(struct node));
	new_node->data = data;
	new_node->next = NULL;
	new_node->arb = NULL;
	return new_node;  
}

void insert(struct node** head_ref, int data)
{
        struct node* new_node = newNode(data);

        new_node->next = (*head_ref);
        (*head_ref) = new_node;
}

void frontBackSplit(struct node* head, struct node** a, struct node** b)
{
	if(head == NULL)
		return;

	struct node* slow = head;
	struct node* fast = head->arb;

	while(fast && fast->arb)
	{
		slow = slow->arb;
		fast = fast->arb->arb;
	}
	*a = head;
	*b = slow->arb;
	slow->arb = NULL;
}

struct node* mergeList(struct node* a, struct node* b)
{
	struct node* head = NULL;	

	if(a == NULL)
		return b;

	if(b == NULL)
		return a;

	if(a->data < b->data)
	{
		head = a;
		head->arb = mergeList(a->arb, b);	
	}
	else
	{
		head = b;
		head->arb = mergeList(a, b->arb); 
	}
	return head;
	
}

void mergeSort(struct node** headRef)
{
	if(*headRef == NULL || (*headRef)->arb == NULL)
		return;

	struct node* head = (*headRef);
	struct node* a = NULL;
	struct node* b = NULL; 
	frontBackSplit(head, &a, &b);

	mergeSort(&a);
	mergeSort(&b);

	*headRef = mergeList(a,b);
}

void printList(struct node* head)
{
	if(head == NULL)
		return;
	struct node* curr = head;	
	while(curr)
	{
		printf(" %d,", curr->data);
		curr = curr->arb; 
	}
}

void populateArb(struct node** head)
{
	if((*head) == NULL || (*head)->next == NULL)
		return;	
	struct node* curr = *head;
	while(curr)
	{
		curr->arb = curr->next;
		curr = curr->next;
	} 

	mergeSort(head);
}

struct node* clone(struct node* head)
{
	if(head == NULL)
		return NULL;

	struct node* curr = head;
	struct node* new_node;
	
	while(curr)
	{
		new_node = newNode(curr->data);
		new_node->next = curr->next;
		curr->next = new_node;
		curr = new_node->next;
	}	

	curr = head;
	
	while(curr)
	{
		if(curr->next && aurr->arb)
		{	
			curr->next->arb = curr->arb->next;
			curr = curr->next->next;
		}
	}

	struct node* clonehead = head->next; 
	curr = head;
	struct node* temp = curr->next;
	
	while(curr->next)
	{
		curr->next = curr->next->next;
		temp->next = temp->next->next;
		curr = curr->next;
		temp = temp->next;
	}
	return clonehead;
}

void main()
{
	struct node* head = NULL;
        insert(&head, 6);
        insert(&head, 4);
        insert(&head, 9);
        insert(&head, 5);
        insert(&head, 7);
        //printList(head);

        struct node* head1 = NULL;
        insert(&head1, 10);
        insert(&head1, 99);
        insert(&head1, 19);
        insert(&head1, 23);
        insert(&head1, 45);
        insert(&head1, 8);
	printf("\n\n");
	populateArb(&head1);
	printList(head1);


}	
